const TEMPLATE = `
    <div class="qikipedia-body qikipedia-theme-{{theme}}">
        <div class="qikipedia-image" style="background:url('{{image}}') no-repeat center center; background-size: cover; height:{{imageHeight}}px"></div>
        {{{body}}}
        <div class="bottom-fade"/>
    </div>
    <div class="qikipedia-footer">
        <a style="color: #59ABE3" href="{{articleUrl}}" target="_blank">${chrome.i18n.getMessage("readMore")}</a>
    </div>
`;